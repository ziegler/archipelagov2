﻿//vector_2d.cc Edgar Aellen and Mathieu Ziegler on Branch Master

#include<iostream>

#include "vector_2d.h"

Vector_2d::Vector_2d(const double &tmp_x,const double &tmp_y)
{
    x=tmp_x;
    y=tmp_y;
}

Vector_2d::Vector_2d()
{}

string Vector_2d::get_string() const
{
    return to_string(x)+string(" ")+to_string(y);
}

Vector_2d operator+(const Vector_2d &vec1,const Vector_2d &vec2)
{
    return Vector_2d (vec1.x+vec2.x, vec1.y+vec2.y);
}

Vector_2d operator-(const Vector_2d &vec1,const Vector_2d &vec2)
{
    return Vector_2d (vec1.x-vec2.x, vec1.y-vec2.y);
}

Vector_2d operator*(const Vector_2d &vec,const double &num)
{
    return Vector_2d(vec.x*num, vec.y*num);
}

Vector_2d operator*(const double &num, const Vector_2d &vec)
{
    return vec*num;
}

Vector_2d operator/(const Vector_2d &vec,const double &num)
{
    return Vector_2d(vec.x/num, vec.y/num);
}

Vector_2d operator/(const double &num, const Vector_2d &vec)
{
    return Vector_2d(num/vec.x, num/vec.y);
}

Vector_2d perpendicular(const Vector_2d &vec)
{
    return Vector_2d (vec.y, -vec.x);
}

bool operator ==(const Vector_2d &vec1,const Vector_2d &vec2)
{
    double delta=0.00001;
    bool result_x=((vec1.x<vec2.x+delta)and(vec1.x>vec2.x-delta));
    bool result_y=((vec1.y<vec2.y+delta)and(vec1.y>vec2.y-delta));
    return (result_x and result_y);
}

double operator*(const Vector_2d &vec1, const Vector_2d &vec2)
{
    return vec1.x*vec2.x+vec1.y*vec2.y;
}
