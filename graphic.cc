﻿//circle.cc Edgar Aellen and Mathieu Ziegler on Branch Master
#include<iostream>

#include "graphic.h"
#include"gui.h"

//local const
namespace {
    constexpr float selected_color[3]={1,0,0};
    constexpr float short_path_color[3]={0,1,0};
    constexpr float def_color[3]={0,0,0};
    constexpr float background_color[3]={1,1,1};

    Vector_2d press_point={0,0};
}

void Graphic::draw_city(const Cairo::RefPtr<Cairo::Context> &cr)
{
    vector<Node*> path_nodes;
    vector<Link*> path_links;
    if((gui->get_shortest_path_pressed()) and (selected_node!=nullptr))
    {
        if(selected_node->get_type()==res)
        {
            path_nodes = selected_node->sp_get_nodes(gui->get_city()->get_nodes());
            path_links = selected_node->sp_get_links(gui->get_city()->get_nodes());
        }
    }

    for(auto node : city()->get_nodes())
    {
        draw_node(node, path_nodes, cr);
    }
    for(auto link : city()->get_links())
    {
        draw_link(link, path_links, cr);
    }
}

void Graphic::draw_link(Link* link, const vector<Link*>& path_links,
                        const Cairo::RefPtr<Cairo::Context> &cr)
{
    Segment new_seg=get_segment_local(link->get_seg());
    Vector_2d new_start=new_seg.get_start();
    new_start=new_start+ new_seg.get_u_vec()*
        get_radius_local(link->get_node_start()->get_circle());
    Vector_2d new_end=new_seg.get_end();
    new_end=new_end- new_seg.get_u_vec()*
        get_radius_local(link->get_node_end()->get_circle());
    cr->set_source_rgb(def_color[0],def_color[1],def_color[2]);

    for(unsigned int i(0); i < path_links.size(); ++i)
    {
        if(link == path_links[i])
        {
            cr->set_source_rgb(short_path_color[0],short_path_color[1],
                               short_path_color[2]);
            break;
        }
    }
    draw_line({new_start, new_end}, cr);
    cr->stroke();
}

void Graphic::draw_node(Node* node, const vector<Node*>& path_nodes,
                        const Cairo::RefPtr<Cairo::Context> &cr)
{
    cr->set_source_rgb(def_color[0],def_color[1],def_color[2]);
    if(node==selected_node)
    {
       cr->set_source_rgb(selected_color[0],selected_color[1],selected_color[2]);
    }

    for(unsigned int i(0); i < path_nodes.size(); ++i)
    {
       if(node == path_nodes[i])
       {
           cr->set_source_rgb(short_path_color[0],short_path_color[1],
                              short_path_color[2]);
           break;
       }
    }

    switch (node->get_type())
    {
       case res:
       {
           draw_residential(node, cr);
           break;
       }
       case trans:
       {
           draw_transport(node, cr);
           break;
       }
       case indus:
       {
           draw_industrial(node, cr);
           break;
       }
    }
    cr->stroke();
}

void Graphic::draw_residential(Node* node, const Cairo::RefPtr<Cairo::Context> &cr)
{
    draw_circle(node->get_circle(), cr);
}

void Graphic::draw_transport(Node* node, const Cairo::RefPtr<Cairo::Context> &cr)
{
    draw_circle(node->get_circle(), cr);
    double radius= get_radius_local(node->get_circle());
    Vector_2d center= get_vector_local(node->get_center());
    cr->move_to(center.x+radius, center.y);
    cr->line_to(center.x-radius, center.y);
    cr->move_to(center.x, center.y+radius);
    cr->line_to(center.x, center.y-radius);
    cr->move_to(center.x+radius*sqrt2, center.y+radius*sqrt2);
    cr->line_to(center.x-radius*sqrt2, center.y-radius*sqrt2);
    cr->move_to(center.x+radius*sqrt2, center.y-radius*sqrt2);
    cr->line_to(center.x-radius*sqrt2, center.y+radius*sqrt2);
}

void Graphic::draw_industrial(Node* node, const Cairo::RefPtr<Cairo::Context> &cr)
{
    draw_circle(node->get_circle(), cr);
    double radius= get_radius_local(node->get_circle());
    Vector_2d center= get_vector_local(node->get_center());
    cr->move_to(center.x+0.75*radius, center.y+radius/8.);
    cr->line_to(center.x-0.75*radius, center.y+radius/8.);
    cr->line_to(center.x-0.75*radius, center.y-radius/8.);
    cr->line_to(center.x+0.75*radius, center.y-radius/8.);
    cr->line_to(center.x+0.75*radius, center.y+radius/8.);
    cr->line_to(center.x-0.75*radius, center.y+radius/8.);
}

void Graphic::draw_circle(Circle circle, const Cairo::RefPtr<Cairo::Context> &cr)
{
    Circle l_circle=get_circle_local(circle);
    cr->move_to(l_circle.get_x()+l_circle.get_radius(), l_circle.get_y());
    cr->arc(l_circle.get_x(), l_circle.get_y(), l_circle.get_radius(),0., 6.281);
}

void Graphic::draw_line(Segment seg, const Cairo::RefPtr<Cairo::Context> &cr)
{
    cr->move_to(seg.get_start().x, seg.get_start().y);
    cr->line_to(seg.get_end().x, seg.get_end().y);
}

Node* Graphic::detect_node_clicked(Vector_2d point)
{
    for(auto node: city()->get_nodes())
    {
        if(contact(node->get_circle(), point))
        {
            return node;
        }
    }
    return nullptr;
}

void Graphic::left_click(Vector_2d point)
{
    Node *tmp_node=detect_node_clicked(point);
    if(tmp_node==nullptr)
    {
        if(selected_node==nullptr)
        {
            city()->create_default_node(point, gui->get_node_type());
        }
    }
    else if(gui->get_edit_link() and selected_node!= nullptr)
    {
        Node* c_node=detect_node_clicked(point);
        vector<Node *> con_nodes=selected_node->get_node_connections();
        Link* current_link = selected_node->find_link(c_node);
        if(current_link!=nullptr)
        {
            city()->destroy_link(current_link);
        }
        else if(current_link==nullptr and c_node !=nullptr)
        {
            city()->create_link(selected_node, c_node);
        }
    }
    else
    {
        if(tmp_node==selected_node)
        {
            city()->destroy_node(selected_node);
            selected_node=nullptr;
        }
        else selected_node=tmp_node;
    }
    press_point=point;
    gui->update();
}

void Graphic::left_release(Vector_2d point)
{
    Node *tmp_node=detect_node_clicked(point);
    if(tmp_node==nullptr and point==press_point) selected_node=nullptr;
    if(selected_node != nullptr and not gui->get_edit_link())
    {
        Segment seg1(selected_node->get_center(), point);
        Segment seg2(selected_node->get_center(), press_point);
        double delta_radius=seg1.get_length()-seg2.get_length();
        double new_radius=selected_node->get_radius();
        new_radius=new_radius+delta_radius;
        city()->change_node_radius(new_radius, selected_node->get_uid());
    }
    gui->update();
}

void Graphic::right_click(Vector_2d point)
{
    if(selected_node!=nullptr)
    {
        city()->move_node_to(point, selected_node->get_uid());
    }
    gui->update();
}

City* Graphic::city()
{
    return gui->get_city();
}

double Graphic::get_x_local(Vector_2d center)
{
    return center.x*gui->get_zoom()/default_zoom_def+width/2.;
}

double Graphic::get_y_local(Vector_2d center)
{
    return -center.y*gui->get_zoom()/default_zoom_def+height/2.;
}

double Graphic::get_x_external(Vector_2d center)
{
    return (center.x-width/2.)/gui->get_zoom()*default_zoom_def;
}

double Graphic::get_y_external(Vector_2d center)
{
    return (-center.y+height/2.)/gui->get_zoom()*default_zoom_def;
}

Vector_2d Graphic::get_vector_local(Vector_2d point)
{
    return Vector_2d(get_x_local(point), get_y_local(point));
}

Vector_2d Graphic::get_vector_external(Vector_2d point)
{
    return Vector_2d(get_x_external(point), get_y_external(point));
}

Circle Graphic::get_circle_local(Circle circle)
{
    return Circle(get_vector_local(circle.get_center()), get_radius_local(circle));
}

Segment Graphic::get_segment_local(Segment seg)
{
    return Segment(get_vector_local(seg.get_start()), get_vector_local(seg.get_end()));
}

double Graphic::get_radius_local(Circle circle)
{
    return get_radius_local(circle.get_radius());
}

double Graphic::get_radius_external(Circle circle)
{
    return get_radius_external(circle.get_radius());
}

double Graphic::get_radius_local(double radius)
{
    return radius*gui->get_zoom()/default_zoom_def;
}

double Graphic::get_radius_external(double radius)
{
    return radius/gui->get_zoom()*default_zoom_def;
}

Graphic::Graphic(Gui *tmp_gui)
{
    add_events(Gdk::BUTTON_PRESS_MASK | Gdk::BUTTON_RELEASE_MASK);
    gui=tmp_gui;
}

void Graphic::draw()
{
    auto win = get_window();
    if(win)
    {
        Gdk::Rectangle r(0,0, get_allocation().get_width(),
                         get_allocation().get_height());
        win->invalidate_rect(r,false);
    }
}

bool Graphic::on_draw(const Cairo::RefPtr<Cairo::Context> &cr)
{
    Gtk::Allocation allocation = get_allocation();
    width = allocation.get_width();
    height = allocation.get_height();
    cr->set_line_width(line_width);
    cr->set_source_rgb(background_color[0],background_color[1],background_color[2]);
    cr->rectangle(0, 0, width, height);
    cr->fill();
    draw_city(cr);
    cr->stroke();

    return true;
}

bool Graphic::on_button_press_event(GdkEventButton* event)
{
    if(event->type != GDK_BUTTON_PRESS) return true;
    try
    {
        Vector_2d point=get_vector_external({event->x, event->y});

        switch (event->button) {
        case 1:
            left_click(point);
            break;
        case 3:
            right_click(point);
            break;
        }
        return true;
    }
    catch(invalid_argument& e)
    {
        cout<<e.what();
        MessageDialog error(*gui, e.what());
        error.run();
        return true;
    }
}

bool Graphic::on_button_release_event(GdkEventButton *event)
{
    if(event->type != GDK_BUTTON_RELEASE) return true;
    try
    {
        Vector_2d point=get_vector_external({event->x, event->y});

        switch (event->button) {
        case 1:
            left_release(point);
            break;
        }
        return true;
    }
    catch(invalid_argument& e)
    {
        cout<<e.what();
        MessageDialog error(*gui, e.what());
        error.run();
        return true;
    }
}
