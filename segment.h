﻿#ifndef SEGMENT_H
#define SEGMENT_H

#include<string>

#include"vector_2d.h"

using namespace std;

class Segment
{
private:

    Vector_2d pt_start;
    Vector_2d pt_end;

public:

    Segment(Vector_2d tmp_start,Vector_2d tmp_end);
    Segment();

    string get_string();
    double get_length() const;
    Vector_2d get_start() const;
    Vector_2d get_end() const;
    Vector_2d get_u_vec() const;
};

#endif // SEGMENT_H
