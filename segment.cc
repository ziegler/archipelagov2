﻿//segment.cc Edgar Aellen and Mathieu Ziegler on Branch Master

#include<cmath>

#include "segment.h"

Segment::Segment(Vector_2d tmp_start,Vector_2d tmp_end)
{
    pt_start=tmp_start;
    pt_end=tmp_end;
}

Segment::Segment()
{}

string Segment::get_string()
{
    return string("Segment \n")+pt_start.get_string()+pt_end.get_string();
}

double Segment::get_length() const
{
    Vector_2d vec_2pt= pt_start-pt_end;
    return sqrt(pow(vec_2pt.x, 2)+pow(vec_2pt.y,2));
}

Vector_2d Segment::get_start() const
{
    return pt_start;
}

Vector_2d Segment::get_end() const
{
    return pt_end;
}

Vector_2d Segment::get_u_vec() const
{
    double length=get_length();
    Vector_2d tmp_vec=pt_end-pt_start;

    if(length!=0)
    {
        tmp_vec=(tmp_vec/length);
    }
    return tmp_vec;
}
