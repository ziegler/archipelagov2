﻿//circle.cc Edgar Aellen and Mathieu Ziegler on Branch Master

#include "circle.h"

Circle::Circle(const Vector_2d &tmp_point, const double &tmp_radius)
{
    center=tmp_point;
    radius=tmp_radius;
}

Circle::Circle()
{

}

Vector_2d Circle::get_center() const
{
    return center;
}

double Circle::get_radius() const
{
    return radius;
}

double Circle::get_x()
{
    return center.x;
}

double Circle::get_y()
{
    return center.y;
}

void Circle::set_center(Vector_2d tmp_center)
{
    center=tmp_center;
}

void Circle::set_radius(double tmp_radius)
{
    radius=tmp_radius;
}

string Circle::get_string()
{
    return string("Circle \nRadius ")+to_string(radius)+string(" ") +
                  center.get_string();
}

