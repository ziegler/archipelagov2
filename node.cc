﻿//node.cc Edgar Aellen and Mathieu Ziegler on Branch Master

#include <algorithm>
#include <iostream>
#include <cmath>

#include "constantes.h"
#include "error.h"
#include "node.h"

std::string node::Node_type_string(Node_type node)
{
    switch(node)
    {
        case res: return "residentiel";
        case trans: return "transport";
        case indus: return "production";
    }
    return "no type";
}

struct Node_coordinates
{
    Node* ptr_node;
    bool in;
    double access;
    Node* parent;
    Link* to_parent;

    Node_coordinates()
        : ptr_node(nullptr), in(true), access(infinite_time), parent(nullptr),
          to_parent(nullptr)
    {}
};

void Node::raise_read_error(string error)
{
    throw invalid_argument(error);
}

vector<Node_coordinates> Node::map_MTA(const vector<Node *>& nodes)
{
    unsigned int size_nodes(nodes.size());
    vector<Node_coordinates> t_access(size_nodes);

    for(unsigned int i(0); i < size_nodes; ++i)
    {
        t_access[i].ptr_node = nodes[i];
        if(nodes[i] == this)
        {
            t_access[i].access = 0;
            swap(t_access.front(), t_access[i]);
        }
    }

    bool found_indus(false), found_trans(false);
    unsigned int t(0);
    while((t < size_nodes) and (t_access[t].access != infinite_time) and
          not((found_indus) and (found_trans)))
    {
        if(t_access[t].ptr_node->type == indus)
        {
            found_indus = true;
        } else if(t_access[t].ptr_node->type == trans) {
            found_trans = true;
        }
        t_access[t].in = false;
        if(not(t_access[t].ptr_node->type == indus)) //It's forbidden to pass
        {                                            //through an industrial node.
            neighbour_MTA(t_access, size_nodes, t);  //Check for the neighbours if
        }                                            //it is best to pass through n.
        ++t;
    }

    return t_access;
}

void Node::neighbour_MTA(vector<Node_coordinates>& t_access,
                         const unsigned int& size_nodes, const unsigned int& t)
{
    for(auto link : t_access[t].ptr_node->connections)
    {
        unsigned int lv(0);
        if(link->get_node_start() == t_access[t].ptr_node)
        {
            while((lv < size_nodes - 1) and
                  (t_access[lv].ptr_node != link->get_node_end()))
            {
                ++lv;
            }
        } else {
            while((lv < size_nodes - 1) and
                  (t_access[lv].ptr_node != link->get_node_start()))
            {
                ++lv;
            }
        }
        if(t_access[lv].in)
        {
            double alt = t_access[t].access + link->travel_time();
            if((t_access[lv].access > alt) or (t_access[lv].access == infinite_time))
            {
                t_access[lv].access = alt;
                t_access[lv].parent = t_access[t].ptr_node;
                t_access[lv].to_parent = link;

                unsigned int j(lv);
                while((j != 0) and (t_access[j-1].access > t_access[j].access))
                {
                    swap(t_access[j-1], t_access[j]);
                    --j;
                }
            }
        }
    }
}

bool Node::check_size()
{
    bool b_return=true;

    if(nbp<min_capacity)
    {
        b_return=false;
        raise_read_error(error::too_little_capacity(nbp));
    }

    if(nbp>max_capacity)
    {
        b_return=false;
        raise_read_error(error::too_much_capacity(nbp));
    }
    return b_return;
}

bool Node::check_uid(const vector<Node *> &nodes)
{
    for(auto node : nodes)
    {
        if(node==this) continue;
        if(uid == node->get_uid())
        {
            raise_read_error(error::identical_uid(uid));
            return false;
        }
    }
    return true;
}

bool Node::check_superposition(const vector<Node *> &nodes,
                               const vector<Link *> &links, bool read)
{
    double dist=dist_min;
    if(read) dist=0;
    for(auto node:nodes)
    {
        if (node==this) continue;

        if(contact(get_circle(), node->get_circle(), dist))
        {
            raise_read_error(error::node_node_superposition(uid, node->get_uid()));
            return false;
        }
    }

    for(auto link:links)
    {
        if(find(connections.begin(), connections.end(), link)!=connections.end())
        {
            continue;
        }

        if(contact(get_circle(), link->get_seg(), dist))
        {
            raise_read_error(error::node_link_superposition(uid));
            return false;
        }
    }
    for(auto cn:connections){
        if(not cn->check_superposition(nodes))
        {
            return false;
        }
    }
    return true;
}

Node::Node(Vector_2d tmp_center, Node_type tmp_type, unsigned int new_uid,
           int tmp_nbp)
{
    uid = new_uid;
    type = tmp_type;
    center=tmp_center;
    nbp=tmp_nbp;
}

Node::Node()
{

}

unsigned int Node::get_uid() const
{
    return uid;
}

unsigned int Node::get_nbp() const
{
    return nbp;
}

double Node::get_radius() const
{
    return sqrt(nbp);
}

node::Node_type Node::get_type() const
{
    return type;
}

bool Node::full_connected() const
{
    if(type == res)
    {
        if(connections.size() == max_link)
        {
            return true;
        }
    }
    return false;
}

Circle Node::get_circle() const
{
    return Circle (center,get_radius());
}

Vector_2d Node::get_center()
{
    return center;
}

vector<Link*> Node::get_connections()
{
    return connections;
}

vector<Node *> Node::get_node_connections()
{
    vector<Node *> con_nodes;
    for(auto con: connections)
    {
        con_nodes.push_back(con->other(this));
    }
    return con_nodes;
}

void Node::set_center(Vector_2d new_center, const vector<Node*> &nodes,
                      const vector<Link*> &links)
{
    Vector_2d old_center=center;
    Node tmp_node();
    center = new_center;
    try
    {
        check_superposition(nodes, links, false);
    }
    catch(invalid_argument& e)
    {
        center=old_center;
        throw invalid_argument(e.what());
    }
}

void Node::set_nbp(unsigned int new_nbp, const vector<Node*> &nodes,
                   const vector<Link*> &links)
{
    unsigned int old_nbp=nbp;
    nbp = new_nbp;

    try
    {
        check(nodes, links);
    }
    catch(invalid_argument& e)
    {
        nbp=old_nbp;
        throw invalid_argument(e.what());
    }
}

void Node::set_radius(double new_radius, const vector<Node *> &nodes,
                      const vector<Link *> &links)
{
    set_nbp((pow(new_radius, 2)+0.5), nodes, links);
}

void Node::add_connection(Link* ptr_new_link)
{
    connections.push_back(ptr_new_link);
}

void Node::rmv_connection(Link* ptr_link)
{
    unsigned int n(connections.size());

    for(unsigned int i=0; i<n; i++)
    {
        if(connections[i] == ptr_link)
        {
            connections.erase(connections.begin()+i);
            return;
        }
    }
}

std::string Node::get_string() const
{
    string space=" ";
    return to_string(uid)+space+ center.get_string()+space + to_string(nbp);
}

bool Node::check(const vector<Node *> &nodes, const vector<Link *> &links, bool read)
{
    bool chk_uid=check_uid(nodes);
    bool chk_size=check_size();
    bool chk_sup=check_superposition(nodes, links, read);

    return chk_uid and chk_size and chk_sup;
}

Link *Node::find_link(Node *node)
{
    for(auto link: get_connections())
    {
        if(link->other(this)==node) return link;
    }
    return nullptr;
}

double Node::node_MTA(const vector<Node*>& nodes)
{
    vector<Node_coordinates> map(map_MTA(nodes));
    unsigned int i(0);
    double mta_trans(0), mta_indus(0);

    while((i < nodes.size()) and (map[i].ptr_node->type != trans)
          and (map[i].access != infinite_time))
    {
        ++i;
    }
    if(i < nodes.size())
    {
        mta_trans = map[i].access;
    } else {
        mta_trans = infinite_time;
    }
    i = 0;
    while((i < nodes.size()) and (map[i].ptr_node->type != indus)
          and (map[i].access != infinite_time))
    {
        ++i;
    }
    if(i < nodes.size())
    {
        mta_indus = map[i].access;
    } else {
        mta_indus = infinite_time;
    }
    return mta_indus + mta_trans;
}

vector<Node_coordinates> Node::node_shortest_path(const vector<Node *> &nodes)
{
    vector<Node_coordinates> map(map_MTA(nodes));
    vector<Node_coordinates> path;

    unsigned int i(1), i_trans(0), i_indus(0);
    while((i < nodes.size()) and ((i_trans == 0) or (i_indus == 0))
          and (map[i].access != infinite_time))
    {
        if((i_indus == 0) and (map[i].ptr_node->type == indus))
        {
            i_indus = i;
        }
        else if((i_trans == 0) and (map[i].ptr_node->type == trans))
        {
            i_trans = i;
        }
        ++i;
    }
    if(i_trans != 0)
    {
        unsigned int j(i_trans - 1);
        do {
            path.push_back(map[i_trans]);
            while(map[j].ptr_node != map[i_trans].parent) {--j;}
            i_trans = j;
        } while(map[i_trans].ptr_node != this);
    }
    if(i_indus != 0)
    {
        unsigned int j(i_indus - 1);
        do {
            path.push_back(map[i_indus]);
            while(map[j].ptr_node != map[i_indus].parent) {--j;}
            i_indus = j;
        } while(map[i_indus].ptr_node != this);
    }
    return path;
}

vector<Node*> Node::sp_get_nodes(const vector<Node *> &nodes)
{
    vector<Node_coordinates> tab(node_shortest_path(nodes));
    vector<Node*> sp_nodes;
    for(auto element : tab)
    {
        sp_nodes.push_back(element.ptr_node);
    }
    return sp_nodes;
}

vector<Link*> Node::sp_get_links(const vector<Node *> &nodes)
{
    vector<Node_coordinates> tab(node_shortest_path(nodes));
    vector<Link*> sp_links;
    for(auto element : tab)
    {
        sp_links.push_back(element.to_parent);
    }
    return sp_links;
}
