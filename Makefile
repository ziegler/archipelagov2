OUT = projet
CXX = g++
DEFINES= -DRENDU1
LINKING=`pkg-config --cflags gtkmm-3.0`
LDLIBS=`pkg-config --libs gtkmm-3.0`
CXXFLAGS = -Wall -g -std=c++11
CXXFILES = vector_2d.cc segment.cc circle.cc tools.cc node.cc link.cc error.cc city.cc gui.cc main.cc graphic.cc
OFILES =  node.o segment.o circle.o vector_2d.o link.o gui.o error.o tools.o city.o graphic.o main.o
ifeq ($(OS),Windows_NT)
#stuff to do if WINDOWS
	RM=@del
	LINKING += -pthread -mms-bitfields -IC:/msys64/mingw64/include/gtkmm-3.0 -IC:/msys64/mingw64/lib/gtkmm-3.0/include -IC:/msys64/mingw64/include/atkmm-1.6 -IC:/msys64/mingw64/include/gdkmm-3.0 -IC:/msys64/mingw64/lib/gdkmm-3.0/include -IC:/msys64/mingw64/include/giomm-2.4 -IC:/msys64/mingw64/lib/giomm-2.4/include -IC:/msys64/mingw64/include/pangomm-1.4 -IC:/msys64/mingw64/lib/pangomm-1.4/include -IC:/msys64/mingw64/include/glibmm-2.4 -IC:/msys64/mingw64/lib/glibmm-2.4/include -IC:/msys64/mingw64/include/gtk-3.0 -IC:/msys64/mingw64/include/cairo -IC:/msys64/mingw64/include -IC:/msys64/mingw64/include/pango-1.0 -IC:/msys64/mingw64/include/fribidi -IC:/msys64/mingw64/include -IC:/msys64/mingw64/include/atk-1.0 -IC:/msys64/mingw64/include/cairo -IC:/msys64/mingw64/include/cairomm-1.0 -IC:/msys64/mingw64/lib/cairomm-1.0/include -IC:/msys64/mingw64/include/cairo -IC:/msys64/mingw64/include/pixman-1 -IC:/msys64/mingw64/include -IC:/msys64/mingw64/include/freetype2 -IC:/msys64/mingw64/include -IC:/msys64/mingw64/include/harfbuzz -IC:/msys64/mingw64/include/libpng16 -IC:/msys64/mingw64/include/sigc++-2.0 -IC:/msys64/mingw64/lib/sigc++-2.0/include -IC:/msys64/mingw64/include/gdk-pixbuf-2.0 -IC:/msys64/mingw64/include -IC:/msys64/mingw64/lib/libffi-3.2.1/include -IC:/msys64/mingw64/include/glib-2.0 -IC:/msys64/mingw64/lib/glib-2.0/include -IC:/msys64/mingw64/include
           LDLIBS += -LC:/msys64/mingw64/lib -lgtkmm-3.0 -latkmm-1.6 -lgdkmm-3.0 -lgiomm-2.4 -lpangomm-1.4 -lglibmm-2.4 -lgtk-3 -lgdk-3 -lz -lgdi32 -limm32 -lshell32 -lole32 -Wl,-luuid -lwinmm -ldwmapi -lsetupapi -lcfgmgr32 -lpangowin32-1.0 -lpangocairo-1.0 -lpango-1.0 -latk-1.0 -lcairo-gobject -lgio-2.0 -lcairomm-1.0 -lcairo -lsigc-2.0 -lgdk_pixbuf-2.0 -lgobject-2.0 -lglib-2.0 -lintl				
else 
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Linux)
#stuff to do if Linux
		RM=@/bin/rm
		LINKING = `pkg-config --cflags gtkmm-3.0`
		LDLIBS = `pkg-config --libs gtkmm-3.0`
	endif
	ifeq ($(UNAME_S),Darwin)
		#stuff to do if OSX
		RM= @rm
		LINKING = `pkg-config --cflags gtkmm-3.0`
		LDLIBS = `pkg-config --libs gtkmm-3.0`
	
	endif
endif
CXXFLAGS+=$(LINKING)
# Definition de la premiere regle

$(OUT): $(OFILES)
	$(CXX) $(OFILES)  $(LDLIBS) $(LINKING) -o $(OUT)

# Definitions de cibles particulieres

depend:
	@echo " *** MISE A JOUR DES DEPENDANCES ***"
	@(sed '/^# DO NOT DELETE THIS LINE/q' Makefile && \
	  $(CXX) -MM $(CXXFILES)| \
	  egrep -v "/usr/include" \
	) >Makefile.new
	@mv Makefile.new Makefile


clean:
	@echo " *** EFFACE MODULES OBJET ET EXECUTABLE ***"
	$(RM) -f *.o *.x *.cc~ *.h~ $(OUT)


# DO NOT DELETE THIS LINE
vector_2d.o: vector_2d.cc vector_2d.h
segment.o: segment.cc segment.h vector_2d.h
circle.o: circle.cc circle.h vector_2d.h
tools.o: tools.cc tools.h constantes.h segment.h vector_2d.h circle.h
node.o: node.cc constantes.h error.h node.h link.h tools.h segment.h \
 vector_2d.h circle.h
link.o: link.cc link.h tools.h constantes.h segment.h vector_2d.h \
 circle.h error.h node.h
error.o: error.cc error.h constantes.h
city.o: city.cc tools.h constantes.h segment.h vector_2d.h circle.h \
 error.h city.h node.h link.h
gui.o: gui.cc gui.h graphic.h city.h node.h link.h tools.h constantes.h \
 segment.h vector_2d.h circle.h error.h
main.o: main.cc gui.h graphic.h city.h node.h link.h tools.h constantes.h \
 segment.h vector_2d.h circle.h error.h
graphic.o: graphic.cc graphic.h city.h node.h link.h tools.h constantes.h \
 segment.h vector_2d.h circle.h error.h gui.h
