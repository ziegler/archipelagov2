﻿#ifndef CITY_H
#define CITY_H

#include <vector>
#include <string>
#include <fstream>

#include "node.h"
#include "link.h"

using namespace node;

class City
{
private:

    vector<Node*> nodes;
    vector<Link*> links;
    string file_name;

    Node* get_node_ptr(unsigned int uid);

    void read_node(string line, Node_type type);
    void read_link(string line);
    void raise_read_error(string error);

    bool status_uid(unsigned int uid);
    unsigned int find_uid();

    string parse_word(string word);

public:

    City();
    City(string tmp_file_name);

    unsigned int get_nodes_nb() const;

    void empty();
    void replace();
    void replace(string file_name);

    void create_link(Node *node_start, Node *node_end, bool read=false);
    void create_link(unsigned int uid1, unsigned int uid2, bool read=false);
    void create_node(Vector_2d center, Node_type type, unsigned int new_uid,
                     unsigned int nbp, bool read=false);
    void create_default_node(Vector_2d center, Node_type type);
    void destroy_link(Link* ptr_link);
    void destroy_node(Node* ptr_node);

    double indicator_ENJ();
    double indicator_CI();
    double indicator_MTA();

    void save(string path) const;

    void print() const;
    void move_node_to(Vector_2d new_center,unsigned int uid);
    void change_node_radius(double new_radius, unsigned uid);

    vector<Node *> get_nodes() const;
    vector<Link *> get_links() const;
};

#endif // CITY_H
