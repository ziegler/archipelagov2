﻿//link.cc Edgar Aellen and Mathieu Ziegler on Branch Master

#include<iostream>

#include "link.h"

void Link::raise_read_error(string error)
{
    throw invalid_argument(error);
}

bool Link::check_link()
{
    if(ptr_node_start == ptr_node_end)
    {
        raise_read_error(error::self_link_node(ptr_node_start->get_uid()));
        return false;
    }

    vector<Link*> local_links= ptr_node_start->get_connections();

    for(auto link: local_links)
    {
        if(equal(link))
        {
            raise_read_error(error::multiple_same_link(ptr_node_start->get_uid(),
                                                       ptr_node_end->get_uid()));
            return false;
        }
    }

    if(ptr_node_start->full_connected())
    {
        raise_read_error(error::max_link(ptr_node_start->get_uid()));
        return false;
    }

    if(ptr_node_end->full_connected())
    {
        raise_read_error(error::max_link(ptr_node_end->get_uid()));
        return false;
    }
    return true;
}

Link::Link(Node* node_start, Node* node_end)
{
    ptr_node_start = node_start;
    ptr_node_end = node_end;

    if (not((node_start->get_type() == trans) and (node_end->get_type() == trans)))
    {
        speed = default_speed;
    }
    else {
        speed = fast_speed;
    }
}

Link::Link()
{

}

Segment Link::get_seg() const
{
    return {ptr_node_start->get_center(), ptr_node_end->get_center()};
}

Node* Link::get_node_start() const
{
    return ptr_node_start;
}

Node* Link::get_node_end() const
{
    return ptr_node_end;
}

Node *Link::other(Node *node) const
{
    if(node == ptr_node_end)
    {
        return ptr_node_start;
    }

    return ptr_node_end;
}

double Link::get_speed() const
{
    return speed;
}

unsigned int Link::get_capacity() const
{
    if(ptr_node_start->get_nbp() < ptr_node_end->get_nbp()) {
        return ptr_node_start->get_nbp();
    } else {
        return ptr_node_end->get_nbp();
    }
}

double Link::travel_time() const
{
    return get_seg().get_length()/speed;
}

bool Link::check(const vector<Node *> &nodes, bool read)
{
    return check_link() and check_superposition(nodes, read);
}

bool Link::equal(Link *link) const
{
    bool b_return= false;
    b_return=b_return or (link->get_node_end()==ptr_node_end and
                          link->get_node_start()==ptr_node_start);
    b_return=b_return or (link->get_node_end()==ptr_node_start and
                          link->get_node_start()==ptr_node_end);
    return b_return;
}

bool Link::check_superposition(const vector<Node *> &nodes, bool read)
{
   double dist=dist_min;
   if(read) dist=0;
    for(auto node:nodes)
    {
        if(node->get_uid()==ptr_node_end->get_uid() or
                node->get_uid()==ptr_node_start->get_uid())
        {
            continue;
        }

        if(contact(node->get_circle(),get_seg(), dist))
        {
            raise_read_error(error::node_link_superposition(node->get_uid()));
            return false;
        }
    }
    return true;
}

std::string Link::get_string()
{
    return to_string(ptr_node_start->get_uid())+string(" ") +
            to_string(ptr_node_end->get_uid());
}

