﻿#ifndef CIRCLE_H
#define CIRCLE_H

#include<string>

#include"vector_2d.h"

using namespace std;

class Circle
{
private:

    Vector_2d center;
    double radius;

public:

    Circle(const Vector_2d &tmp_point,const double &tmp_radius);
    Circle();

    Vector_2d get_center() const;
    double get_radius() const;

    double get_x();
    double get_y();
    void set_center(Vector_2d tmp_center);
    void set_radius(double tmp_radius);
    string get_string();

};

#endif // CIRCLE_H
