﻿#ifndef NODE_H
#define NODE_H

#include <vector>
#include <string>

namespace node
{
    enum Node_type {res=0, trans=1, indus=2};
    std::string Node_type_string(Node_type node);
}

using namespace node;

class Node;
#include "link.h"

struct Node_coordinates;

class Node
{
private:

    unsigned int uid;
    Vector_2d center;
    vector<Link*> connections;
    Node_type type;
    unsigned int nbp;

    void raise_read_error(string error);

    vector<Node_coordinates> map_MTA(const vector<Node*>& nodes);
    void neighbour_MTA(vector<Node_coordinates>& t_access,
                       const unsigned int& size_nodes, const unsigned int& t);

    bool check_size();
    bool check_uid(const vector<Node*> &nodes);
    bool check_superposition(const vector<Node*> &nodes, const vector<Link*> &links,
                             bool read=false);

public:

    Node(Vector_2d tmp_center, Node_type tmp_type, unsigned int new_uid, int tmp_nbp);
    Node();

    unsigned int get_uid() const;
    unsigned int get_nbp() const;
    double get_radius() const;
    Node_type get_type() const;
    bool full_connected() const; //This function check if we can build another link.
    Circle get_circle() const;
    Vector_2d get_center();
    vector<Link*> get_connections();
    vector<Node*> get_node_connections();

    void set_center(Vector_2d new_center, const vector<Node*> &nodes,
                    const vector<Link*> &links);
    void set_nbp(unsigned int new_nbp, const vector<Node*> &nodes,
                 const vector<Link*> &links);
    void set_radius(double new_radius, const vector<Node*> &nodes,
                 const vector<Link*> &links);
    void add_connection(Link* ptr_new_link);
    void rmv_connection(Link* ptr_link);

    string get_string() const;

    bool check(const vector<Node*> &nodes, const vector<Link*> &links,
               bool read=false);
    Link* find_link(Node* node);

    double node_MTA(const vector<Node*>& nodes);

    vector<Node_coordinates> node_shortest_path(const vector<Node*>& nodes);
    vector<Node*> sp_get_nodes(const vector<Node *> &nodes);
    vector<Link*> sp_get_links(const vector<Node *> &nodes);
};

#endif // NODE_H
