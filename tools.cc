﻿//tools.cc Edgar Aellen and Mathieu Ziegler on Branch Master

#include <cmath>

#include "tools.h"


bool contact(Circle c1, Circle c2, double dist_min)
{
    Segment seg(c1.get_center(), c2.get_center());
    if(c1.get_radius()+c2.get_radius()+dist_min>seg.get_length())
    {
        return true;
    }
    return false;
}

bool contact(Circle circle, Segment seg, double dist_min)
{
    Segment seg_start(seg.get_start(),circle.get_center());
    double scal_start=seg_start.get_u_vec()*seg.get_u_vec();

    if(scal_start<0.0)
    {
        if(seg_start.get_length()>=circle.get_radius()+dist_min)
        {
            return false;
        }
    }

    Segment seg_end(seg.get_end(),circle.get_center());
    double scal_end=seg_end.get_u_vec()*seg.get_u_vec()*(-1);

    if(scal_end<0.0)
    {
        if(seg_end.get_length()>=circle.get_radius()+dist_min)
        {
            return false;
        }
    }

    Vector_2d perp_vect=perpendicular(seg.get_u_vec());
    double distance=abs(perp_vect * seg_start.get_u_vec())*seg_start.get_length();
    if(distance>=circle.get_radius()+dist_min)
    {
        return false;
    }

    if(seg.get_end()==seg.get_start()){
        Segment dist_to_center(circle.get_center(), seg.get_start());
        return dist_to_center.get_length()<circle.get_radius()+dist_min;
    }
    return true;
}

bool contact(Circle c1, Vector_2d point)
{
    Segment seg(c1.get_center(), point);
    return (seg.get_length()<c1.get_radius());
}
