﻿//main.cc Edgar Aellen and Mathieu Ziegler on Branch Master

#include <string>

#include "gui.h"

using namespace std;

int main(int argc, char* argv[])
{
    int argc_screen=1;//commentaire
    auto app = Gtk::Application::create(argc_screen, argv);
    Gui eventWindow;
    if(argc>1)
    {
        std::string file_name(argv[1]);
        eventWindow.open_city(file_name);
    }
    return app->run(eventWindow);
}
