﻿//gui.cc Edgar Aellen and Mathieu Ziegler on Branch Master

#include <sstream>
#include <iomanip>

#include "gui.h"
#include "constantes.h"

constexpr int border_width(5);

void Gui::box_setup()
{
    set_title("Archipelago");
    set_border_width(border_width);
    add(main_Box);
    general_setup();
    display_setup();
    editor_setup();
    info_setup();
    graphic_setup();
    l_Box.add(general);
    l_Box.add(display);
    l_Box.add(editor);
    l_Box.add(info);
    main_Box.pack_start(l_Box, false, false);
    main_Box.pack_start(graph);
}

void Gui::general_setup()
{
    button_exit.signal_clicked().connect(sigc::mem_fun(*this, &Gui::on_exit_pressed));
    button_new.signal_clicked().connect(sigc::mem_fun(*this, &Gui::on_new_pressed));
    button_open.signal_clicked().connect(sigc::mem_fun(*this, &Gui::on_open_pressed));
    button_save.signal_clicked().connect(sigc::mem_fun(*this, &Gui::on_save_pressed));

    button_exit.set_label("exit");
    button_new.set_label("new");
    button_open.set_label("open");
    button_save.set_label("save");
    l_general_Box.add(button_exit);
    l_general_Box.add(button_new);
    l_general_Box.add(button_open);
    l_general_Box.add(button_save);
    general.add(l_general_Box);
    general.set_label("General");
}

void Gui::display_setup()
{
    button_zoom_in.signal_clicked().connect(sigc::mem_fun(*this, &Gui::zoom_plus));
    button_zoom_out.signal_clicked().connect(sigc::mem_fun(*this, &Gui::zoom_minus));
    button_zoom_reset.signal_clicked().connect(sigc::mem_fun(*this, &Gui::zoom_reset));
    button_shorthest_path.signal_clicked().connect(sigc::mem_fun(*this, &Gui::update));

    button_shorthest_path.set_label("shortest path");
    button_zoom_in.set_label("zoom in");
    button_zoom_out.set_label("zoom out");
    button_zoom_reset.set_label("zoom reset");
    label_zoom.set_label(string("zoom: x")+get_dec_string(zoom_value,2));
    l_display_Box.add(button_shorthest_path);
    l_display_Box.add(button_zoom_in);
    l_display_Box.add(button_zoom_out);
    l_display_Box.add(button_zoom_reset);
    l_display_Box.add(label_zoom);
    display.add(l_display_Box);
    display.set_label("Display");
}

void Gui::editor_setup()
{
    button_edit_link.set_label("edit link");
    button_housing.set_label("housing");
    button_transport.set_label("transport");
    button_production.set_label("production");

    button_housing.set_group(group_type);
    button_transport.set_group(group_type);
    button_production.set_group(group_type);
    l_editor_Box.add(button_edit_link);
    l_editor_Box.add(button_housing);
    l_editor_Box.add(button_transport);
    l_editor_Box.add(button_production);
    editor.add(l_editor_Box);
    editor.set_label("Editor");
}

void Gui::info_setup()
{
    label_enj.set_label("ENJ : " + get_dec_string(city.indicator_ENJ(), 4));
    label_ci.set_label("CI : " + get_dec_string(city.indicator_CI(), 0));
    label_mta.set_label("MTA : " + get_dec_string(city.indicator_MTA(), 0));
    l_info_Box.add(label_enj);
    l_info_Box.add(label_ci);
    l_info_Box.add(label_mta);
    info.add(l_info_Box);
    info.set_label("Informations");
}

void Gui::graphic_setup()
{
    mygraphic.set_size_request(default_drawing_size, default_drawing_size);
    graph.set_border_width(border_width);
    graph.add(mygraphic);
}

std::string Gui::get_dec_string(double value, int dec) const
{
    std::stringstream stream;
    stream << std::fixed << std::setprecision(dec) << value;
    return stream.str();
}

std::string Gui::get_adapted_notation_string(double value) const
{
    std::ostringstream stream;
    stream << value;
    return stream.str();
}

Gui::Gui() :
    main_Box(ORIENTATION_HORIZONTAL,3),
    l_Box(ORIENTATION_VERTICAL, 3),
    l_general_Box(ORIENTATION_VERTICAL, 3),
    l_display_Box(ORIENTATION_VERTICAL, 3),
    l_editor_Box(ORIENTATION_VERTICAL, 3),
    l_info_Box(ORIENTATION_VERTICAL, 3)
{
    set_resizable(true);
    box_setup();
    show_all_children();
}

Gui::~Gui()
{

}

double Gui::get_zoom() const
{
    return zoom_value;
}

node::Node_type Gui::get_node_type() const
{
    if(button_housing.get_active()) return res;
    if(button_transport.get_active()) return trans;
    if(button_production.get_active()) return indus;
    return res;
}

City* Gui::get_city()
{
    return &city;
}

bool Gui::get_edit_link()
{
    return button_edit_link.get_active();
}

bool Gui::get_shortest_path_pressed()
{
    return button_shorthest_path.get_active();
}

void Gui::open_city(string file_name)
{
    city.replace(file_name);
    update();
}

void Gui::update()
{
    label_enj.set_label("ENJ : " + get_adapted_notation_string(city.indicator_ENJ()));
    label_ci.set_label("CI : " + get_adapted_notation_string(city.indicator_CI()));
    label_mta.set_label("MTA : " + get_adapted_notation_string(city.indicator_MTA()));
    label_zoom.set_label(string("zoom: x")+get_dec_string(zoom_value,2));
    mygraphic.draw();
    show_all_children();
}

void Gui::on_exit_pressed()
{
    exit(0);
}

void Gui::on_new_pressed()
{
    city.replace();
    update();
}

void Gui::on_open_pressed()
{
    FileChooserDialog fc("Please choose a folder", Gtk::FILE_CHOOSER_ACTION_OPEN);
    fc.set_transient_for(*this);
    fc.add_button("Cancel", Gtk::RESPONSE_CANCEL);
    fc.add_button("Select", Gtk::RESPONSE_OK);
    int result = fc.run();
    if(result==RESPONSE_OK) city.replace(fc.get_filename());
    update();
}

void Gui::on_save_pressed()
{
    FileChooserDialog sv("Please choose a folder", Gtk::FILE_CHOOSER_ACTION_SAVE);
    sv.set_transient_for(*this);
    sv.add_button("Cancel", Gtk::RESPONSE_CANCEL);
    sv.add_button("Save", Gtk::RESPONSE_OK);
    int result = sv.run();
    if(result==RESPONSE_OK)
    {
        city.save(sv.get_filename());
    }
}

bool Gui::on_key_press_event(GdkEventKey *event)
{
    switch (event->keyval) {
    case 105:
        zoom_plus();
        break;
    case 111:
        zoom_minus();
        break;
    }
    return true;
}

void Gui::zoom_plus()
{
    if(zoom_value+delta_zoom/2<=max_zoom)
    {
        zoom_value+=delta_zoom;
        update();
    }
}

void Gui::zoom_minus()
{
    if(zoom_value-delta_zoom/2>=min_zoom)
    {
        zoom_value-=delta_zoom;
        update();
    }
}

void Gui::zoom_reset()
{
    zoom_value=1.;
    update();
}
