﻿#ifndef LINK_H
#define LINK_H

#include<string>

#include "tools.h"
#include "error.h"
#include "constantes.h"

class Link; //forward declaration

#include "node.h"

using namespace node;

class Link
{
private:

    Node* ptr_node_start;
    Node* ptr_node_end;
    double speed;

    void raise_read_error(string error);
    bool check_link();

public:

    Link(Node* node_start, Node* node_end);
    Link();

    Segment get_seg() const;
    Node* get_node_start() const;
    Node* get_node_end() const;
    Node* other(Node* node) const;
    double get_speed() const;
    unsigned int get_capacity() const;
    double travel_time() const;

    bool check(const vector<Node*> &nodes, bool read=false);
    bool equal(Link* link) const;
    bool check_superposition(const vector<Node*> &nodes,bool read=false);
    std::string get_string();
};

#endif // LINK_H
