﻿#ifndef TOOLS_H
#define TOOLS_H

#include "segment.h"
#include "circle.h"

using namespace std;

bool contact(Circle c1, Circle c2, double dist_min=0);
bool contact(Circle circle, Segment seg, double dist_min=0);
bool contact(Circle c1, Vector_2d point);

#endif //TOOLS_H

