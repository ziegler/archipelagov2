TEMPLATE = app
CONFIG += console c++11
QMAKE_CXXFLAGS_RELEASE += -O3
CONFIG -= app_bundle
CONFIG -= qt



SOURCES += city.cc error.cc link.cc node.cc circle.cc segment.cc \
                                vector_2d.cc main.cc \
    gui.cc \
    graphic.cc \
    tools.cc

HEADERS+=city.h error.h link.h node.h circle.h tools.h segment.h \
                                vector_2d.h main.h constantes.h \
    gui.h \
    graphic.h



win32{
    #Windef here

}

unix{

        macx {
            QT_CONFIG -= no-pkg-config
            PKG_CONFIG=/usr/local/bin/pkg-config
                # Mac OS X definitions should be entered here ...
        }
         else {
                # Linux definitions go here ...
        }
    CONFIG	+= link_pkgconfig
    PKGCONFIG += gtkmm-3.0
}




