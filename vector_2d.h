﻿#ifndef VECTOR_2D_H
#define VECTOR_2D_H

#include<string>

using namespace std;

struct Vector_2d
{
    double x;
    double y;

    Vector_2d(const double &tmp_x, const double &tmp_y);
    Vector_2d();

    string get_string() const;
};

#endif // VECTOR_2D_H

//not in namespace because basic operator
Vector_2d operator+(const Vector_2d &vec1, const Vector_2d &vec2);
Vector_2d operator-(const Vector_2d &vec1, const Vector_2d &vec2);
Vector_2d operator*(const Vector_2d &vec, const double &num);
Vector_2d operator/(const Vector_2d &vec, const double &num);
Vector_2d operator*(const double &num, const Vector_2d &vec);
Vector_2d operator/(const double &num, const Vector_2d &vec);
Vector_2d perpendicular(const Vector_2d &vec);
bool operator ==(const Vector_2d &vec1, const Vector_2d &vec2);
double operator*(const Vector_2d &vec1,const Vector_2d &vec2);




