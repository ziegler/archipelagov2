﻿#ifndef GUI_H
#define GUI_H

#include<string>
#include <gtkmm.h>

#include"graphic.h"
#include"node.h"
#include"city.h"

using namespace Gtk;

class Gui : public Window
{
private:

    double zoom_value=1;
    node::Node_type node_type=node::res;
    City city;

    void box_setup();
    void general_setup();
    void display_setup();
    void editor_setup();
    void info_setup();
    void graphic_setup();

    std::string get_dec_string(double value, int dec) const;
    std::string get_adapted_notation_string(double value) const;

    Box main_Box,l_Box,l_general_Box, l_display_Box, l_editor_Box, l_info_Box;
    Button button_exit, button_new, button_save, button_zoom_in,
    button_zoom_out, button_zoom_reset, button_open;
    ToggleButton button_edit_link, button_shorthest_path;
    Frame general, display, editor, info, graph;
    Label label_zoom, label_enj, label_ci, label_mta;
    RadioButton button_housing, button_transport, button_production;
    RadioButtonGroup group_type;
    Graphic mygraphic{this};

public:

    Gui();
    virtual ~Gui();

    double get_zoom() const;
    node::Node_type get_node_type() const;
    City* get_city();
    bool get_edit_link();
    bool get_shortest_path_pressed();
    void open_city(string file_name);
    void update();

protected:

    void on_exit_pressed();
    void on_new_pressed();
    void on_open_pressed();
    void on_save_pressed();

    bool on_key_press_event(GdkEventKey* event) override;

    void zoom_plus();
    void zoom_minus();
    void zoom_reset();
};

#endif // GUI_H
