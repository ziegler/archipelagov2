﻿#ifndef GRAPHIC_H
#define GRAPHIC_H

#include<gtkmm/drawingarea.h>

#include"city.h"
#include"tools.h"

class Gui;

class Graphic : public Gtk::DrawingArea
{
private:

    Gui *gui;
    int height;
    int width;
    Node* selected_node=nullptr;

    void draw_city(const Cairo::RefPtr<Cairo::Context> &cr);

    void draw_link(Link* link, const vector<Link*>& path_links,
                   const Cairo::RefPtr<Cairo::Context> &cr);
    void draw_node(Node *node, const vector<Node*>& path_nodes,
                   const Cairo::RefPtr<Cairo::Context> &cr);

    void draw_residential(Node *node, const Cairo::RefPtr<Cairo::Context> &cr);
    void draw_transport(Node *node, const Cairo::RefPtr<Cairo::Context> &cr);
    void draw_industrial(Node *node, const Cairo::RefPtr<Cairo::Context> &cr);

    void draw_circle(Circle circle, const Cairo::RefPtr<Cairo::Context> &cr);
    void draw_line(Segment seg, const Cairo::RefPtr<Cairo::Context> &cr);

    Node* detect_node_clicked(Vector_2d point);

    void left_click(Vector_2d point);
    void left_release(Vector_2d point);
    void right_click(Vector_2d point);

    City *city();

    double get_x_local(Vector_2d center);
    double get_y_local(Vector_2d center);
    double get_x_external(Vector_2d center);
    double get_y_external(Vector_2d center);
    Vector_2d get_vector_local(Vector_2d point);
    Vector_2d get_vector_external(Vector_2d point);
    Circle get_circle_local(Circle circle);
    Segment get_segment_local(Segment seg);
    double get_radius_local(Circle circle);
    double get_radius_external(Circle circle);
    double get_radius_local(double radius);
    double get_radius_external(double radius);

public:

    Graphic(Gui *tmp_gui);
    void draw();

protected:

    bool on_draw(const Cairo::RefPtr<Cairo::Context>& tmp_cr) override;
    bool on_button_press_event(GdkEventButton* event) override;
    bool on_button_release_event(GdkEventButton* event) override;
};


#endif // GRAPHIC_H
