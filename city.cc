﻿//city.cc Edgar Aellen and Mathieu Ziegler on Branch Master

#include <sstream>
#include <iostream>

#include "tools.h"
#include "error.h"
#include "city.h"

Node *City::get_node_ptr(unsigned int uid)
{
    int size=nodes.size();

    for(int i=0; i<size; i++)
    {
        if(nodes[i]->get_uid()==uid)
        {
            return nodes[i];
        }
    }
    raise_read_error(error::link_vacuum(uid));
    return nullptr;
}

void City::read_node(string line, Node_type type)
{
    string word;
    istringstream string_line(line);
    unsigned int uid;
    string_line>>word;
    try
    {
       unsigned long long_uid=stoul(word);
       unsigned int test_uint=-2;
       if(long_uid>test_uint) raise_read_error(error::reserved_uid());

       uid=long_uid;
    }
    catch(out_of_range)
    {
        raise_read_error(error::reserved_uid());
    }
    string_line>>word;
    double posx=stod(word);
    string_line>>word;
    double posy=stod(word);
    string_line>>word;
    int nbp=stoi(word);
    Vector_2d pt(posx, posy);
    create_node(pt, type, uid , nbp, true);
}

void City::read_link(string line)
{
    string word;
    istringstream string_line(line);
    string_line>>word;
    unsigned int uid[2];
    uid[0]=stoul(word);
    string_line>>word;
    uid[1]=stoul(word);
    create_link(uid[0],uid[1], true);
}

void City::raise_read_error(string error)
{
    throw invalid_argument(error);
}

bool City::status_uid(unsigned int uid)
{
    for(auto node: nodes)
    {
        if(node->get_uid()==uid)
        {
            return false;
        }
    }
    return true;
}

unsigned int City::find_uid()
{
    for(unsigned int i=0; i<(unsigned int)-1; i++)
    {
        if(status_uid(i))
        {
            return i;
        }
    }
    return 0;
}

string City::parse_word(string word)
{
    unsigned int i=0;
    for(;i<word.size();i++)
    {
        if(word[i]=='#')
            break;
    }
    word.resize(i);
    return word;
}

City::City()
{}

City::City(string tmp_file_name)
{
    file_name=tmp_file_name;
    ifstream in_file(tmp_file_name);
    if(in_file.fail())
    {
        raise_read_error(string("file not found ")+file_name);
        return;
    }
    string line;
    string word;
    int n=0;
    int i=0;
    try
    {
        while(getline(in_file, line) and i<3)
        {
            line=parse_word(line);
            if((line=="")or (line=="\t")) continue;
            istringstream string_line(line);
            string_line>>word;
            n=stoi(word);
            for(int j=0; j<n;)
            {
                getline(in_file, line);
                line=parse_word(line);
                if((line=="")or (line=="\t")) continue;;
                read_node(line,Node_type(i));
                j++;
            }
            i++;
            if(i==3) break;
        }
        while(getline(in_file, line))
        {
            line=parse_word(line);
            if((line=="")or (line=="\t")) continue;
            istringstream string_line(line);
            string_line>>word;
            n=stoi(word);
            for(int j=0; j<n;)
            {
                getline(in_file, line);
                line=parse_word(line);
                if((line=="")or (line=="\t")) continue;
                read_link(line);
                j++;
            }
            break;
        }
        in_file.close();
    }
    catch(invalid_argument& e)
    {
        cout<<e.what()<<endl;
        for(auto node:nodes)
        {
            delete node;
        }
        for(auto link:links)
        {
            delete link;
        }
        nodes.clear();
        links.clear();
        return;
    }
    cout<<error::success();
}

unsigned int City::get_nodes_nb() const
{
    return nodes.size();
}

void City::empty()
{
    for(auto link: links)
    {
        delete link;
    }
    for(auto node: nodes)
    {
        delete node;
    }
    file_name="";
}

void City::replace()
{
    empty();
    *this=City();
}

void City::replace(string file_name)
{
    empty();
    *this={file_name};
}

void City::create_link(Node* node_start, Node* node_end, bool read)
{
    Link* ptr_new_link = new Link(node_start, node_end);

    if(not ptr_new_link->check(nodes, read)){
        delete ptr_new_link;
        return;
    }

    node_start->add_connection(ptr_new_link);
    node_end->add_connection(ptr_new_link);

    links.push_back(ptr_new_link);
}

void City::create_link(unsigned int uid1, unsigned int uid2, bool read)
{
    create_link(get_node_ptr(uid1), get_node_ptr(uid2), read);
}

void City::create_node(Vector_2d center, Node_type type, unsigned int new_uid,
                       unsigned int nbp, bool read)
{
    Node* ptr_new_node = new Node(center, type, new_uid, nbp);

    if(!ptr_new_node->check(nodes, links, read))
    {
        delete ptr_new_node;
    }
    else
    {
        nodes.push_back(ptr_new_node);
    }
}

void City::create_default_node(Vector_2d center, Node_type type)
{
    create_node(center, type, find_uid(), min_capacity);
}

void City::destroy_link(Link* ptr_link)
{
    (ptr_link->get_node_start())->rmv_connection(ptr_link);
    (ptr_link->get_node_end())->rmv_connection(ptr_link);
    unsigned int n(links.size());

    for(unsigned int i(0); i < n; ++i)
    {
        if(links[i] == ptr_link)
        {
            swap(links[i],links[links.size()-1]);
            links.pop_back();
            delete ptr_link;
            return;
        }
    }
}

void City::destroy_node(Node* ptr_node)
{
    unsigned int nb_connections = ptr_node->get_connections().size();

    if(nb_connections > 0)
    {
        for(int i(nb_connections - 1); i >= 0; --i)
        {
            destroy_link(ptr_node->get_connections()[i]);
        }
    }

    unsigned int n(nodes.size());

    for(size_t i(0); i < n; ++i)
    {
        if(nodes[i] == ptr_node)
        {
            swap(nodes[i],nodes[nodes.size()-1]);
            nodes.pop_back();
            delete ptr_node;
            return;
        }
    }
}

double City::indicator_ENJ()
{
    if(get_nodes_nb() == 0)
    {
        return 0;
    }

    double daytime_nbp(0), nighttime_nbp(0);

    for(size_t i(0); i < get_nodes_nb(); ++i)
    {
        if(nodes[i]->get_type() == res)
        {
            nighttime_nbp += nodes[i]->get_nbp();
        } else {
            daytime_nbp += nodes[i]->get_nbp();
        }
    }

    return (nighttime_nbp - daytime_nbp)/(daytime_nbp + nighttime_nbp);
}

double City::indicator_CI()
{
    unsigned int n = links.size();

    if(n == 0)
    {
        return 0;
    }

    double costs(0);

    for(size_t i(0); i < n; ++i)
    {
        costs += links[i]->get_seg().get_length() * links[i]->get_capacity() *
                 links[i]->get_speed();
    }
    return costs;
}

double City::indicator_MTA()
{
    if(get_nodes_nb() == 0)
    {
        return 0.;
    }
    double nb_res(0);
    double total;

    for(unsigned int i(0); i < get_nodes_nb(); ++i)
    {
        if(nodes[i]->get_type() == res)
        {
            ++nb_res;
            total = total + nodes[i]->node_MTA(nodes);
        }
    }
    return total/nb_res;
}

void City::save(string path) const
{
    ofstream out_file;
    out_file.open(path);
    out_file << "# file generated by the best programm"<< endl << endl << endl;
    for(int i=0; i<3; i++)
    {
        Node_type nd=(Node_type)i;
        unsigned int num=0;
        for(auto node: nodes)
        {
            if(node->get_type()==nd) num++;
        }
        out_file <<"# nb " << Node_type_string(nd) << endl << num << endl
                << "# " << Node_type_string(nd) << endl;
        for(auto node: nodes)
        {
            if(node->get_type()==nd)
            {
                out_file<<"\t"<< node->get_string()<<endl;
            }
        }
        out_file<<endl;
    }
    out_file<<"# nb "<<"link"<<endl<<links.size()<<endl<<"# link"<<endl;
    for(auto link:links)
    {
        out_file<<"\t"<<link->get_string()<<endl;
    }
    out_file.close();
}


void City::print() const
{
    int size=nodes.size();
    cout<<"City"<<endl;
    for(int i=0; i<size; i++)
    {
        cout<<nodes[i]->get_string()<<endl;;
    }
    cout<<endl;
}

void City::move_node_to(Vector_2d new_center,unsigned int uid)
{
    Node* ptr=get_node_ptr(uid);
    ptr->set_center(new_center, nodes, links);
}

void City::change_node_radius(double new_radius, unsigned uid)
{
    Node* ptr=get_node_ptr(uid);
    ptr->set_radius(new_radius, nodes, links);
}

vector<Node *> City::get_nodes() const
{
    return nodes;
}

vector<Link *> City::get_links() const
{
    return links;
}
